﻿#if LOG
using System;

namespace PG
{
    public enum LogEvent
    {
        [String("debug")]
        [ConsoleColor(ConsoleColor.White)]
        Debug,
        [String("event")]
        [ConsoleColor(ConsoleColor.Red)]
        Event
    }
}
#endif
