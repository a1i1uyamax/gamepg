﻿#if LOG
using static System.Console;

namespace PG
{
    public static class Logger
    {
        public static void Log(LogEvent logEvent, string message, GameObject obj = null, params object[] args)
        {
            BackgroundColor = logEvent.GetConsoleColorValue();
            ForegroundColor = System.ConsoleColor.Black;
            Write($"{logEvent.GetStringValue().ToUpperInvariant()}:");
            ResetColor();

            if (obj != null)
                Write($" ({obj.GetType().Name}, id: {obj.Id}, name: {obj.Name}) ");

            if (args?.Length > 0)
                WriteLine(message, args);
            else
                WriteLine(message);
        }
    }
}
#endif
