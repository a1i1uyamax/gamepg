﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed class OptionsMenu : IEnumerable<OptionView>
    {
        private readonly List<OptionView> views = new();

        public bool IsActive => views.Any();

        public void SetOptions(Vector2 startPoint, params OptionView[] views)
        {
            this.views.AddRange(views);
            for (int i = 0; i < this.views.Count; i++)
            {
                var view = this.views[i];
                view.Position = new(startPoint.X, i * view.Texture.Height + startPoint.Y);
            }
        }

        public void Clear() => views.Clear();

        public void Draw(
#if DEBUG
            bool shouldShowCollisionBoxes,
#endif
            SpriteBatch spriteBatch
        )
        {
            views.ForEach(view =>
                view.Draw(
#if DEBUG
                    shouldShowCollisionBoxes,
#endif
                    spriteBatch
                )
            );
        }

        public IEnumerator<OptionView> GetEnumerator()
            => views.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => views.GetEnumerator();
    }
}
