﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PG
{
    public sealed class InventoryMenu : IDisposable
    {
        private const string AssetNameFormat = "menu/inventory/{0}";

        private readonly Texture2D backgroundTexture;
        private readonly Texture2D weaponColumnTexture;
        private readonly ReadOnlyIndents inventoryIndents;
        private readonly SpriteFont itemFont;
        private readonly OptionView removeFromBackpackOptionView;
        private readonly OptionView removeWeaponOptionView;
        private readonly OptionView addWeaponOptionView;
        private readonly OptionsMenu optionsMenu;

        private IconView backpackView;
        private readonly List<IconView> itemViews = new();
        private IconView weaponView;

        private Rectangle menuRect;
        private CharactersInventory inventory;
#if DEBUG
        private bool isStarted = false;
#endif

        private InventoryMenu(Texture2D backgroundTexture,
                              Texture2D weaponColumnTexture,
                              Texture2D optionTexture,
                              SpriteFont optionFont,
                              SpriteFont itemFont,
                              CharacterMenuParams menuParams)
        {
            this.backgroundTexture = backgroundTexture;
            this.weaponColumnTexture = weaponColumnTexture;
            this.itemFont = itemFont;
            inventoryIndents = new(menuParams.InventoryIndents);
            removeFromBackpackOptionView = new(optionTexture, "Remove item", optionFont);
            removeWeaponOptionView = new(optionTexture, "Disarm", optionFont);
            addWeaponOptionView = new(optionTexture, "Arm", optionFont);
            optionsMenu = new();
        }

        public void Update(MouseState mouseState, Camera camera)
        {
            var relativeMousePosition = camera.GetRelativeMousePosition(mouseState);

            if (mouseState.RightButton == ButtonState.Pressed && !optionsMenu.IsActive)
            {
                var selectedItemView = itemViews.FirstOrDefault(item =>
                    item.GetTextureRect()
                        .Contains(relativeMousePosition)
                );

                if (selectedItemView != null)
                {
                    if (selectedItemView.Type == IconView.ItemType.Weapon)
                    {
                        optionsMenu.SetOptions(
                            relativeMousePosition,
                            removeFromBackpackOptionView.SetItemId(selectedItemView.Id),
                            addWeaponOptionView.SetItemId(selectedItemView.Id)
                        );
                    }
                    else
                    {
                        optionsMenu.SetOptions(
                            relativeMousePosition,
                            removeFromBackpackOptionView.SetItemId(selectedItemView.Id)
                        );
                    }
                }
                else if (weaponView?.GetTextureRect().Contains(relativeMousePosition) ?? false)
                {
                    if (inventory.Backpack != null)
                    {
                        optionsMenu.SetOptions(
                            relativeMousePosition,
                            removeFromBackpackOptionView.SetItemId(weaponView.Id),
                            removeWeaponOptionView.SetItemId(weaponView.Id)
                        );
                    }
                    else
                    {
                        optionsMenu.SetOptions(
                            relativeMousePosition,
                            removeFromBackpackOptionView.SetItemId(weaponView.Id)
                        );
                    }
                }
            }
            else if (mouseState.LeftButton == ButtonState.Pressed)
            {
                if (optionsMenu.IsActive)
                {
                    var option = optionsMenu.FirstOrDefault(option =>
                        option.GetTextureRect()
                              .Contains(relativeMousePosition)
                    );

                    if (option == removeFromBackpackOptionView)
                    {
                        if (option.ItemId == inventory.Weapon?.Id)
                        {
                            weaponView = null;
                        }
                        else
                            itemViews.Remove(itemViews.First(view => view.Id == option.ItemId));

                        inventory.DropItem(option.ItemId);
                    }
                    else if (option == removeWeaponOptionView)
                    {
                        inventory.Disarm();
                        var view = weaponView;
                        weaponView = null;
                        view.Position = (itemViews.LastOrDefault()?.Position
                            ?? new(menuRect.X + inventoryIndents.Left, menuRect.Y + inventoryIndents.Top))
                                + new Vector2(view.Texture.Width, 0f);
                        itemViews.Add(view);
                    }
                    else if (option == addWeaponOptionView)
                    {
                        inventory.Arm();
                        weaponView = new(inventory.Weapon)
                        {
                            Position = new(
                                menuRect.X + menuRect.Width - inventoryIndents.Right - inventory.Weapon.IconTexture.Width,
                                menuRect.Y + inventoryIndents.Top
                            )
                        };
                        itemViews.Remove(itemViews.First(view => view.Id == option.ItemId));
                    }

                    optionsMenu.Clear();
                    removeFromBackpackOptionView.Clear();
                    removeWeaponOptionView.Clear();
                    addWeaponOptionView.Clear();
                }
            }
        }

        public void Draw(
#if DEBUG
            bool shouldShowCollisionBoxes,
#endif
            SpriteBatch spriteBatch
        )
        {
            spriteBatch.Draw(
                backgroundTexture,
                menuRect,
                Color.White
            );
            spriteBatch.Draw(
                weaponColumnTexture,
                menuRect,
                Color.White
            );

            weaponView?.Draw(
#if DEBUG
                shouldShowCollisionBoxes,
#endif
                spriteBatch,
                itemFont
            );
            backpackView?.Draw(
#if DEBUG
                shouldShowCollisionBoxes,
#endif
                spriteBatch,
                itemFont
            );
            itemViews.ForEach(item =>
                item.Draw(
#if DEBUG
                    shouldShowCollisionBoxes,
#endif
                    spriteBatch,
                    itemFont
                )
            );

            if (optionsMenu.IsActive)
            {
                optionsMenu.Draw(
#if DEBUG
                    shouldShowCollisionBoxes,
#endif
                    spriteBatch
                );
            }
        }

        public void Start(Camera camera, Character character)
        {
#if DEBUG
            if (isStarted)
                throw new InvalidOperationException("End() method was not executed!");

            isStarted = true;
#endif
            menuRect = camera.VisibleArea;
            inventory = character.Inventory;

            if (inventory.Backpack != null)
            {
                backpackView = new(inventory.Backpack)
                {
                    Position = new(
                        menuRect.X + menuRect.Width - inventoryIndents.Right - inventory.Backpack.IconTexture.Width,
                        menuRect.Y + menuRect.Height - inventoryIndents.Bottom - inventory.Backpack.IconTexture.Height
                    )
                };

                var itemViews = inventory.Backpack.Items.Select(item =>
                    new IconView(item)
                    {
                        Position = new(
                            menuRect.X + inventoryIndents.Left,
                            menuRect.Y + inventoryIndents.Top
                        )
                    }
                ).ToArray();

                for (int i = 1; i < itemViews.Length; i++)
                {
                    itemViews[i].Position += new Vector2(
                        menuRect.X - itemViews[i - 1].Position.X + itemViews[i - 1].Texture.Width,
                        0f
                    );
                }

                this.itemViews.AddRange(itemViews);
            }

            if (inventory.Weapon != null)
            {
                weaponView = new(inventory.Weapon)
                {
                    Position = new(
                        menuRect.X + menuRect.Width - inventoryIndents.Right - inventory.Weapon.IconTexture.Width,
                        menuRect.Y + inventoryIndents.Top
                    )
                };
            }
        }

        public void End()
        {
#if DEBUG
            isStarted = false;
#endif
            menuRect = Rectangle.Empty;
            inventory = null;
            itemViews.Clear();
            weaponView = null;
            backpackView = null;
        }

        public static InventoryMenu Load(ContentManager content)
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            static string GetAssetNameFormatted(string name)
                => string.Format(AssetNameFormat, name); 

            var characterMenuParams = JsonParser.ParseFromContentFile<CharacterMenuParams>(content, GetAssetNameFormatted("menu"));
            var backgroundTexture = content.Load<Texture2D>(GetAssetNameFormatted("inventory_background"));
            var weaponColumnTexture = content.Load<Texture2D>(GetAssetNameFormatted("weapon_column"));
            var optionTexture = content.Load<Texture2D>(GetAssetNameFormatted("option"));
            var optionFont = content.Load<SpriteFont>(GetAssetNameFormatted("option_font"));
            var itemFont = content.Load<SpriteFont>(GetAssetNameFormatted("item_font"));

            return new(
                backgroundTexture,
                weaponColumnTexture,
                optionTexture,
                optionFont,
                itemFont,
                characterMenuParams
            );
        }

        public void Dispose() => End();
    }
}
