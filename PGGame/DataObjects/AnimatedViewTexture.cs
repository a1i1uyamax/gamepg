﻿using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed class AnimatedViewTexture
    {
        public Texture2D Texture { get; }
        public AnimatedViewInfo ViewInfo { get; }

        public AnimatedViewTexture(Texture2D texture, AnimatedViewInfo viewInfo)
            => (Texture, ViewInfo) = (texture, viewInfo);
    }
}
