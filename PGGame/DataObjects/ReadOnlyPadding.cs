﻿namespace PG
{
    public sealed class ReadOnlyPadding
    {
        public int Left { get; }
        public int Right { get; }
        public int Top { get; }
        public int Bottom { get; }
        public int Vertical => Top + Bottom;
        public int Horizontal => Left + Right;

        public ReadOnlyPadding(Padding padding)
        {
            Left = padding.Left;
            Right = padding.Right;
            Top = padding.Top;
            Bottom = padding.Bottom;
        }
    }
}
