﻿using System;
using System.Linq;
using Microsoft.Xna.Framework.Input;

namespace PG
{
    public sealed class InputKey
    {
        public MouseKeys? MouseKey { get; init; }
        public Keys? KeyboardKey { get; init; }

        private InputKey() { }

        public static InputKey Create(string keyFlag)
        {
            if (Enum.TryParse<Keys>(keyFlag, out var key))
                return new() { KeyboardKey = key };

            return new()
            {
                MouseKey = Enum.GetValues(typeof(MouseKeys))
                               .Cast<MouseKeys>()
                               .First(key => string.Equals(keyFlag, key.GetStringValue()))
            };
        }
    }
}
