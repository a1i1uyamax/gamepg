﻿using System;
using Microsoft.Xna.Framework;

namespace PG
{
    public sealed class FrameInfo
    {
        public Point DefaultFramePosition { get; }
        public int FrameWidth { get; }
        public int FrameHeight { get; }
        public int FramesCount { get; }
        public TimeSpan FrameSetDelay { get; }

        public FrameInfo(AnimatedViewInfo viewInfo)
        {
            DefaultFramePosition = new(
                viewInfo.FrameWidth * viewInfo.DefaultFramePoint.X,
                viewInfo.FrameHeight * viewInfo.DefaultFramePoint.Y
            );
            FrameWidth = viewInfo.FrameWidth;
            FrameHeight = viewInfo.FrameHeight;
            FramesCount = viewInfo.FramesCount;
            FrameSetDelay = TimeSpan.FromMilliseconds(1E+3 / viewInfo.FramesCount);
        }
    }
}
