﻿using System.Runtime.Serialization;
using Microsoft.Xna.Framework;

namespace PG
{
    [DataContract]
    public struct AnimatedViewInfo
    {
        [DataMember(Name = "frameWidth")]
        public int FrameWidth { get; set; }

        [DataMember(Name = "frameHeight")]
        public int FrameHeight { get; set; }

        [DataMember(Name = "framesCount")]
        public int FramesCount { get; set; }

        [DataMember(Name = "padding")]
        public Padding Padding { get; set; }

        [DataMember(Name = "defaultFrame")]
        public Point DefaultFramePoint { get; set; }
    }
}
