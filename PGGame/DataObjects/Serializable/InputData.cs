﻿using System.Runtime.Serialization;

namespace PG
{
    [DataContract]
    public sealed class InputData
    {
        [DataMember(Name = "moveForward")]
        public string MoveForward { get; set; }

        [DataMember(Name = "moveBack")]
        public string MoveBack { get; set; }

        [DataMember(Name = "moveLeft")]
        public string MoveLeft { get; set; }

        [DataMember(Name = "moveRight")]
        public string MoveRight { get; set; }

        [DataMember(Name = "act")]
        public string Act { get; set; }

        [DataMember(Name = "changeMode")]
        public string ChangeMode { get; set; }

        [DataMember(Name = "characterMenu")]
        public string CharacterMenu { get; set; }

        [DataMember(Name = "pause")]
        public string Pause { get; set; }

        [DataMember(Name = "escape")]
        public string Escape { get; set; }

        [IgnoreDataMember]
        public InputIntentPair MoveForwardIntentPair
            => new(InputIntent.MoveForward, MoveForward);

        [IgnoreDataMember]
        public InputIntentPair MoveBackIntentPair
            => new(InputIntent.MoveBack, MoveBack);

        [IgnoreDataMember]
        public InputIntentPair MoveLeftIntentPair
            => new(InputIntent.MoveLeft, MoveLeft);

        [IgnoreDataMember]
        public InputIntentPair MoveRightIntentPair
            => new(InputIntent.MoveRight, MoveRight);

        [IgnoreDataMember]
        public InputIntentPair ActIntentPair
            => new(InputIntent.Act, Act);

        [IgnoreDataMember]
        public InputIntentPair ChangeModeIntentPair
            => new(InputIntent.ChangeMode, ChangeMode);

        [IgnoreDataMember]
        public InputIntentPair CharacterMenuIntentPair
            => new(InputIntent.CharacterMenu, CharacterMenu);

        [IgnoreDataMember]
        public InputIntentPair PauseIntentPair
            => new(InputIntent.Pause, Pause);

        [IgnoreDataMember]
        public InputIntentPair EscapeIntentPair
            => new(InputIntent.Escape, Escape);

#if DEBUG
        [DataMember(Name = "show_collision_boxes")]
        public string ShowCollisionBoxes { get; set; }

        [IgnoreDataMember]
        public InputIntentPair ShowCollisionBoxesPair
            => new(InputIntent.ShowCollisionBoxes, ShowCollisionBoxes);
#endif

        public sealed class InputIntentPair
        {
            public InputIntent InputIntent { get; }
            public string KeyFlag { get; }

            public InputIntentPair(InputIntent inputIntent, string keyFlag)
                => (InputIntent, KeyFlag) = (inputIntent, keyFlag);
        }
    }
}
