﻿using System.Runtime.Serialization;

namespace PG
{
    [DataContract]
    public struct Padding
    {
        [DataMember(Name = "left")]
        public int Left { get; set; }

        [DataMember(Name = "right")]
        public int Right { get; set; }

        [DataMember(Name = "top")]
        public int Top { get; set; }

        [DataMember(Name = "bottom")]
        public int Bottom { get; set; }
    }
}
