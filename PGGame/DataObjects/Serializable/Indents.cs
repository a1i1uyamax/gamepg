﻿using System.Runtime.Serialization;

namespace PG
{
    [DataContract]
    public sealed class Indents
    {
        [DataMember(Name = "indentTop")]
        public int Top { get; set; }

        [DataMember(Name = "indentBottom")]
        public int Bottom { get; set; }

        [DataMember(Name = "indentLeft")]
        public int Left { get; set; }

        [DataMember(Name = "indentRight")]
        public int Right { get; set; }
    }
}
