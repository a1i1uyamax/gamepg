﻿using System.Runtime.Serialization;
using Microsoft.Xna.Framework;

namespace PG
{
    [DataContract]
    public sealed class CharacterMenuParams
    {
        [DataMember(Name = "inventoryStartPosition")]
        public Point InventoryStart { get; set; }

        [DataMember(Name = "inventory")]
        public Indents InventoryIndents { get; set; }
    }
}
