﻿namespace PG
{
    public sealed class ReadOnlyIndents
    {
        public int Top { get; }
        public int Bottom { get; }
        public int Left { get; }
        public int Right { get; }

        public ReadOnlyIndents(Indents indents)
        {
            Top = indents.Top;
            Bottom = indents.Bottom;
            Left = indents.Left;
            Right = indents.Right;
        }
    }
}
