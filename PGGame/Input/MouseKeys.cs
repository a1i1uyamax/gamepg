﻿namespace PG
{
    public enum MouseKeys
    {
        [String("mouseLeft")]
        Left,
        [String("mouseRight")]
        Right,
        [String("mouseMiddle")]
        Middle,
        [String("mouseXButton1")]
        XButton1,
        [String("mouseXButton2")]
        XButton2
    }
}
