﻿using System;

namespace PG
{
    [Flags]
    public enum InputIntent
    {
        MoveForward = 0x1,
        MoveLeft = 0x2,
        MoveRight = 0x4,
        MoveBack = 0x8,
        Act = 0x10,
        ChangeMode = 0x20,
        CharacterMenu = 0x100,
        Pause = 0x800,
        Escape = 0x1000,
#if DEBUG
        ShowCollisionBoxes = 0x2000,
#endif
    }
}
