﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace PG
{
    public sealed class InputManager
    {
        private const string ConfigNameFormat = "/config/{0}";

        private readonly ReadOnlyDictionary<InputIntent, InputKey> inputMapping;

        private InputManager(IDictionary<InputIntent, InputKey> inputMapping)
            => this.inputMapping = new(inputMapping);

        public InputIntent GetIntents(KeyboardState keyboardState, MouseState mouseState)
        {
            InputIntent result = 0x0;
            Enum.GetValues(typeof(InputIntent))
                .Cast<InputIntent>()
                .ToList()
                .ForEach(intent =>
                {
                    var inputKey = inputMapping[intent];
                    if ((inputKey.KeyboardKey.HasValue && keyboardState.IsKeyDown(inputKey.KeyboardKey.Value))
                     || (inputKey.MouseKey.HasValue && mouseState.IsKeyDown(inputKey.MouseKey.Value)))
                    {
                        result |= intent;
                    }
                });
            return result;
        }

        public static InputManager LoadFromConfig(ContentManager content, string fileName)
        {
            var data = JsonParser.ParseFromContentFile<InputData>(content, string.Format(ConfigNameFormat, fileName));
            var inputMapping = new Dictionary<InputIntent, string>
            {
                { data.MoveForwardIntentPair.InputIntent, data.MoveForwardIntentPair.KeyFlag },
                { data.MoveBackIntentPair.InputIntent, data.MoveBackIntentPair.KeyFlag },
                { data.MoveLeftIntentPair.InputIntent, data.MoveLeftIntentPair.KeyFlag },
                { data.MoveRightIntentPair.InputIntent, data.MoveRightIntentPair.KeyFlag },
                { data.ActIntentPair.InputIntent, data.ActIntentPair.KeyFlag },
                { data.ChangeModeIntentPair.InputIntent, data.ChangeModeIntentPair.KeyFlag },
                { data.CharacterMenuIntentPair.InputIntent, data.CharacterMenuIntentPair.KeyFlag },
                { data.PauseIntentPair.InputIntent, data.PauseIntentPair.KeyFlag },
                { data.EscapeIntentPair.InputIntent, data.EscapeIntentPair.KeyFlag },
#if DEBUG
                { data.ShowCollisionBoxesPair.InputIntent, data.ShowCollisionBoxesPair.KeyFlag },
#endif
            }
            .ToDictionary(kvp => kvp.Key, kvp => InputKey.Create(kvp.Value));

            return new(inputMapping);
        }
    }
}
