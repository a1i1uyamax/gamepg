﻿using Microsoft.Xna.Framework;

namespace PG
{
    public static class MathConst
    {
        public static readonly Quaternion LeftTurnQuaternion = Quaternion.CreateFromAxisAngle(Vector3.UnitZ, -MathHelper.PiOver2);
        public static readonly Quaternion RightTurnQuaternion = Quaternion.CreateFromAxisAngle(Vector3.UnitZ, MathHelper.PiOver2);
    }
}
