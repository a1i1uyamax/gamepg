﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PG
{
    public sealed class Camera
    {
        private readonly Vector2 startPosition;
        private readonly CameraZoom zoom;

        public Vector2 Position { get; set; }
        public Rectangle VisibleArea { get; private set; }
        public Matrix Transform { get; private set; }

        public Camera(GraphicsDeviceManager gdManager)
        {
            zoom = new(1f);
            Position = startPosition = new(
                gdManager.PreferredBackBufferWidth / 2f,
                gdManager.PreferredBackBufferHeight / 2f
            );
        }

        public Vector2 GetRelativeMousePosition(MouseState mouseState)
            => new Vector2(mouseState.X, mouseState.Y) + Position - startPosition;

        public void Update(Viewport viewport, MouseState mouseState, Vector2 displacement, Vector2 shift)
        {
            var position = Position + displacement;

            var inverseViewMatrix = Matrix.Invert(Transform);

            var tl = Vector2.Transform(Vector2.Zero, inverseViewMatrix);
            var tr = Vector2.Transform(new(viewport.Bounds.X, 0), inverseViewMatrix);
            var bl = Vector2.Transform(new(0, viewport.Bounds.Y), inverseViewMatrix);
            var br = Vector2.Transform(new(viewport.Bounds.Width, viewport.Bounds.Height), inverseViewMatrix);

            var min = new Vector2(
                MathHelper.Min(tl.X, MathHelper.Min(tr.X, MathHelper.Min(bl.X, br.X))),
                MathHelper.Min(tl.Y, MathHelper.Min(tr.Y, MathHelper.Min(bl.Y, br.Y)))
            );
            var max = new Vector2(
                MathHelper.Max(tl.X, MathHelper.Max(tr.X, MathHelper.Max(bl.X, br.X))),
                MathHelper.Max(tl.Y, MathHelper.Max(tr.Y, MathHelper.Max(bl.Y, br.Y)))
            );

            Transform = Matrix.CreateTranslation(new(-position.X, -position.Y, 0)) *
                        Matrix.CreateScale(zoom.Value) *
                        Matrix.CreateTranslation(new(viewport.Bounds.Width / 2f, viewport.Bounds.Height / 2f, 0));

            VisibleArea = new(
                (int)min.X,
                (int)min.Y,
                (int)(max.X - min.X),
                (int)(max.Y - min.Y)
            );

            Position += shift;

            zoom.Update(mouseState);
        }
    }
}
