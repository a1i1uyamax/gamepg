﻿using Microsoft.Xna.Framework.Input;

namespace PG
{
    public sealed class CameraZoom
    {
        private const float MinValue = .35f;
        private const float MaxValue = 2f;
        private const float Rate = .05f;

        private int previousMouseWheelValue;
        private float value;

        public float Value
        {
            get => value;

            private set
            {
                this.value = value;

                if (this.value < MinValue)
                {
                    this.value = MinValue;
                }
                else if (this.value > MaxValue)
                {
                    this.value = MaxValue;
                }
            }
        }

        public CameraZoom(float value) => this.value = value;

        public void Update(MouseState mouseState)
        {
            var currentMouseWheelValue = mouseState.ScrollWheelValue;

            if (currentMouseWheelValue > previousMouseWheelValue)
            {
                Value += Rate;
            }
            else if (currentMouseWheelValue < previousMouseWheelValue)
            {
                Value -= Rate;
            }

            previousMouseWheelValue = currentMouseWheelValue;
        }
    }
}
