﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public class View
    {
        private readonly Texture2D mainTexture;

        public virtual Texture2D Texture => mainTexture;
        public Vector2 Position { get; set; }
        public Vector2 Origin { get; set; }
        public float Rotation { get; set; }
        public ReadOnlyPadding Padding { get; }

        public View(Texture2D mainTexture, Vector2? origin = null, float rotation = 0, Padding? padding = null)
        {
            this.mainTexture = mainTexture;
            Position = Vector2.Zero;
            Origin = origin ?? Vector2.Zero;
            Rotation = rotation;
            Padding = new(padding ?? new());
        }

        public View(View view)
        {
            mainTexture = view.mainTexture;
            Position = view.Position;
            Origin = view.Origin;
            Rotation = view.Rotation;
        }

        public virtual Rectangle GetTextureRect()
            => new((int)Position.X,
                   (int)Position.Y,
                   mainTexture.Width,
                   mainTexture.Height);

        public virtual Rectangle GetTextureRectWithPaddings()
            => new((int)Position.X + Padding.Left,
                   (int)Position.Y + Padding.Top,
                   mainTexture.Width - Padding.Horizontal,
                   mainTexture.Height - Padding.Vertical);

        public virtual Rectangle GetTextureRect(int additionX, int additionY)
            => new((int)Position.X + additionX,
                   (int)Position.Y + additionY,
                   mainTexture.Width + 2 * additionX,
                   mainTexture.Height + 2 * additionY);

        public Rectangle GetTextureRect(int addition)
            => GetTextureRect(addition, addition);

        public virtual Rectangle GetRectAt(Vector2 position)
            => new((int)position.X,
                   (int)position.Y,
                   mainTexture.Width,
                   mainTexture.Height);

        public Rectangle GetRectWithPaddingAt(Vector2 position)
        {
            var rectAt = GetRectAt(position);
            return new(
                rectAt.X + Padding.Left,
                rectAt.Y + Padding.Top,
                rectAt.Width - Padding.Horizontal,
                rectAt.Height - Padding.Vertical
            );
        }

        public void DrawIfVisible(
#if DEBUG
            bool shouldShowCollisionBoxes,
#endif
            SpriteBatch spriteBatch,
            Camera camera
        )
        {
            if (GetTextureRect().Intersects(camera.VisibleArea))
            {
                Draw(
#if DEBUG
                    shouldShowCollisionBoxes,
#endif
                    spriteBatch
                );
            }
        }

        public virtual void Draw(
#if DEBUG
            bool shouldShowCollisionBoxes,
#endif
            SpriteBatch spriteBatch
        )
        {
            spriteBatch.Draw(mainTexture, Position, Color.White);
#if DEBUG
            if (shouldShowCollisionBoxes)
                DrawCollisionBox(spriteBatch);
#endif
        }

#if DEBUG
        protected void DrawCollisionBox(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                CollisionBox.Instance.Texture,
                Padding == null
                    ? GetTextureRect()
                    : GetTextureRectWithPaddings(),
                Color.White
            );
        }
#endif
    }
}
