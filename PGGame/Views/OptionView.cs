﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed class OptionView : View
    {
        private readonly string text;
        private readonly SpriteFont font;

        public long ItemId { get; private set; }

        public OptionView(Texture2D texture, string text, SpriteFont font) : base(texture)
            => (this.text, this.font, ItemId) = (text, font, IdManager.InvalidId);

        public OptionView SetItemId(long itemId)
        {
            ItemId = itemId;
            return this;
        }

        public override void Draw(
#if DEBUG
            bool shouldShowCollisionBoxes,
#endif
            SpriteBatch spriteBatch
        )
        {
            base.Draw(
#if DEBUG
                shouldShowCollisionBoxes,
#endif
                spriteBatch
            );
            spriteBatch.DrawString(
                font,
                text,
                Position,
                Color.Black
            );
        }

        public void Clear() => ItemId = IdManager.InvalidId;
    }
}
