﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed class IconView : View
    {
        public enum ItemType
        {
            Default,
            Weapon,
            Backpack
        }

        private readonly string itemName;

        public long Id { get; }
        public ItemType Type { get; }

        public IconView(Item item) : base(item.IconTexture)
        {
            Id = item.Id;
            Type = item is Weapon
                ? ItemType.Weapon
                : item is Backpack
                    ? ItemType.Backpack
                    : ItemType.Default;
            itemName = item.Name;
        }

        public void Draw(
#if DEBUG
            bool shouldShowCollisionBoxes,
#endif
            SpriteBatch spriteBatch,
            SpriteFont font
        )
        {
            Draw(
#if DEBUG
                shouldShowCollisionBoxes,
#endif
                spriteBatch
            );
            spriteBatch.DrawString(
                font,
                itemName,
                Position + new Vector2(0f, Texture.Height - font.Texture.Height),
                Color.Black
            );
        }
    }
}
