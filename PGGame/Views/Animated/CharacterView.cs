﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed class CharacterView : AnimatedView
    {
        public enum State
        {
            Default,
            Dead
        }

        private readonly FrameInfo deadFrameInfo;
        private readonly Texture2D deadTexture;

        public State CurrentState { get; set; }

        public override Texture2D Texture => CurrentState switch
        {
            State.Default => base.Texture,
            State.Dead => deadTexture,
            _ => throw new NotSupportedException($"Unknown state: {CurrentState}")
        };

        protected override FrameInfo TextureFrameInfo => CurrentState switch
        {
            State.Default => base.TextureFrameInfo,
            State.Dead => deadFrameInfo,
            _ => throw new NotSupportedException($"Unknown state: {CurrentState}")
        };

        public CharacterView(AnimatedViewTexture mainAnimViewTexture,
                             AnimatedViewTexture deadAnimViewTexture)
            : base(mainAnimViewTexture)
        {
            deadFrameInfo = new(deadAnimViewTexture.ViewInfo);
            deadTexture = deadAnimViewTexture.Texture;
            CurrentState = State.Default;
        }
    }
}
