﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public class AnimatedView : View
    {
        private readonly FrameInfo mainTextureFrameInfo;

        private TimeSpan previousGameTime;
        private Point currentFramePosition;

        protected virtual FrameInfo TextureFrameInfo => mainTextureFrameInfo;

        public AnimatedView(AnimatedViewTexture mainAnimViewTexture)
            : base(mainAnimViewTexture.Texture,
                   new Vector2(mainAnimViewTexture.ViewInfo.FrameWidth / 2f,
                               mainAnimViewTexture.ViewInfo.FrameHeight / 2f),
                   padding: mainAnimViewTexture.ViewInfo.Padding)
        {
            mainTextureFrameInfo = new(mainAnimViewTexture.ViewInfo);
            currentFramePosition = mainTextureFrameInfo.DefaultFramePosition;
        }

        public void ResetTextureRect()
            => currentFramePosition = TextureFrameInfo.DefaultFramePosition;

        public void SetFrame(TimeSpan gameTime)
        {
            if (TextureFrameInfo.FramesCount > 1
                && gameTime - previousGameTime > TextureFrameInfo.FrameSetDelay)
            {
                SetFrame();
                previousGameTime = gameTime;
            }
        }

        public void SetFrame()
        {
            currentFramePosition.X += TextureFrameInfo.FrameWidth;
            if (currentFramePosition.X >= Texture.Width)
            {
                currentFramePosition.X = 0;
                currentFramePosition.Y += TextureFrameInfo.FrameHeight;
                if (currentFramePosition.Y >= Texture.Height)
                    currentFramePosition.Y = 0;
            }
        }

        public override sealed Rectangle GetTextureRect()
            => new((int)Position.X - TextureFrameInfo.FrameWidth / 2,
                   (int)Position.Y - TextureFrameInfo.FrameHeight / 2,
                   TextureFrameInfo.FrameWidth,
                   TextureFrameInfo.FrameHeight);

        public override sealed Rectangle GetTextureRectWithPaddings()
            => new((int)Position.X - TextureFrameInfo.FrameWidth / 2 + Padding.Left,
                   (int)Position.Y - TextureFrameInfo.FrameHeight / 2 + Padding.Top,
                   TextureFrameInfo.FrameWidth - Padding.Horizontal,
                   TextureFrameInfo.FrameHeight - Padding.Vertical);

        public override sealed Rectangle GetTextureRect(int additionX, int additionY)
            => new((int)Position.X - TextureFrameInfo.FrameWidth / 2 + additionX,
                   (int)Position.Y - TextureFrameInfo.FrameHeight / 2 + additionY,
                   TextureFrameInfo.FrameWidth + 2 * additionX,
                   TextureFrameInfo.FrameHeight + 2 * additionY);

        public override sealed Rectangle GetRectAt(Vector2 position)
            => new((int)position.X - TextureFrameInfo.FrameWidth / 2,
                   (int)position.Y - TextureFrameInfo.FrameHeight / 2,
                   TextureFrameInfo.FrameWidth,
                   TextureFrameInfo.FrameHeight);

        public void SetRotation(Vector2 target)
        {
            Rotation = (float)Math.Atan((Position.Y - target.Y) / (Position.X - target.X));
            if (target.X < Position.X)
                Rotation += MathHelper.Pi;
        }

        public override sealed void Draw(
#if DEBUG
            bool shouldShowCollisionBoxes,
#endif
            SpriteBatch spriteBatch
        )
        {
            spriteBatch.Draw(
                Texture,
                Position,
                new(currentFramePosition.X,
                    currentFramePosition.Y,
                    TextureFrameInfo.FrameWidth,
                    TextureFrameInfo.FrameHeight),
                Color.White,
                Rotation,
                Origin,
                1f,
                SpriteEffects.None,
                0f
            );
#if DEBUG
            if (shouldShowCollisionBoxes)
                DrawCollisionBox(spriteBatch);
#endif
        }
    }
}
