﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public class ItemView : AnimatedView
    {
        public enum State
        {
            Idle,
            Picked
        }

        private readonly FrameInfo pickedFrameInfo;
        private readonly Texture2D pickedTexture;

        public State CurrentState { get; set; }

        public Texture2D IconTexture { get; }

        public override Texture2D Texture => CurrentState switch
        {
            State.Idle => base.Texture,
            State.Picked => pickedTexture,
            _ => throw new NotSupportedException($"Unknown state: {CurrentState}")
        };

        protected override FrameInfo TextureFrameInfo => CurrentState switch
        {
            State.Idle => base.TextureFrameInfo,
            State.Picked => pickedFrameInfo,
            _ => throw new NotSupportedException($"Unknown state: {CurrentState}")
        };

        public ItemView(AnimatedViewTexture mainAnimViewTexture,
                        AnimatedViewTexture pickedAnimViewTexture,
                        Texture2D iconTexture)
            : base(mainAnimViewTexture)
        {
            pickedFrameInfo = new(pickedAnimViewTexture.ViewInfo);
            pickedTexture = pickedAnimViewTexture.Texture;
            IconTexture = iconTexture;
            CurrentState = State.Idle;
        }
    }
}
