﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public class GroundView : View
    {
        public int VerticalCount { get; set; }
        public int HorizontalCount { get; set; }

        public GroundView(Texture2D texture, Vector2? origin = null, float rotation = 0)
            : base(texture, origin, rotation)
        {
        }

        public Rectangle GetMapRect()
            => new((int)Position.X, (int)Position.Y, HorizontalCount * Texture.Width, VerticalCount * Texture.Height);

        public override void Draw(
#if DEBUG
            bool _,
#endif
            SpriteBatch spriteBatch
        )
        {
            for (int i = 0; i < HorizontalCount; ++i)
            {
                for (int j = 0; j < VerticalCount; ++j)
                {
                    spriteBatch.Draw(
                        Texture,
                        new Vector2(Position.X + i * Texture.Width, Position.Y + j * Texture.Height),
                        Color.White
                    );
                }
            }
        }
    }
}
