﻿namespace PG
{
    class Program
    {
        private static void Main(string[] _)
        {
            using var theGame = new GamePG();
            theGame.Run();
        }
    }
}
