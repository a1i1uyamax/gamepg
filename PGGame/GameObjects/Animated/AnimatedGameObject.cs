﻿namespace PG
{
    public abstract class AnimatedGameObject : GameObject
    {
        protected readonly AnimatedView animView;

        public AnimatedGameObject(AnimatedView animView, string name, long id) : base(animView, name, id)
            => this.animView = animView;
    }
}
