﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed class Character : AnimatedGameObject
    {
        private readonly CharacterView charView;

        private int health;

        public CharactersInventory Inventory { get; }
        public float Speed { get; set; }
        public int PickUpDistance { get; set; }
        public bool IsAlive => charView.CurrentState is not CharacterView.State.Dead;
        public int Health
        {
            get => health;
            set
            {
#if LOG
                if (value < health)
                    Logger.Log(LogEvent.Event, "character is heart, hit: {0}, health: {1}", this, health - value, health);
#endif
                health = value;
                if (health <= 0)
                {
                    charView.CurrentState = CharacterView.State.Dead;
#if LOG
                    Logger.Log(LogEvent.Event, "character is dead", this);
#endif
                }
            }
        }

        public event EventHandler<ShootingEventArgs> Shoot;
        public event EventHandler<CharactersInventory.DropItemEventArgs> ItemDropped;

        public Character(CharacterView charView, string name, long id)
            : base(charView, name, id)
        {
            this.charView = charView;
            Inventory = new();
            Inventory.ItemDropped += OnItemDropped;
        }

        public void Update(TimeSpan totalGameTime,
                           IEnumerable<GameObject> staticObjects,
                           IEnumerable<Item> idleItems,
                           IController controller)
        {
            switch (charView.CurrentState)
            {
                case CharacterView.State.Default:

                    var newPosition = controller.GetNewPosition(this, staticObjects);
                    if (newPosition != animView.Position)
                    {
                        animView.Position = newPosition;
                        animView.SetFrame(totalGameTime);
                        Inventory.Backpack?.Update(totalGameTime, this);
                    }
                    else
                        animView.ResetTextureRect();

                    animView.SetRotation(controller.Target);

                    if (controller.ShouldChangeMode)
                    {
                        if (Inventory.IsArmed)
                        {
                            if (!Inventory.Disarm())
                                Inventory.DropItem(Inventory.Weapon.Id);
#if LOG
                            else
                                Logger.Log(LogEvent.Debug, "Disarmed", this);
#endif
                        }
                        else if (Inventory.Arm())
                        {
#if LOG
                            Logger.Log(LogEvent.Debug, "Armed", this);
#endif
                        }
                    }

                    if (Inventory.Weapon == null)
                    {
                        if (controller.ShouldAct)
                        {
                            var pickedItem = idleItems.FirstOrDefault(item =>
                            {
                                var itemRect = item.View.GetTextureRect();
                                return itemRect.Intersects(animView.GetTextureRect(PickUpDistance))
                                    && itemRect.Contains(controller.Target);
                            });
                            if (pickedItem != null)
                            {
                                if (Inventory.Backpack == null && pickedItem is Weapon weapon)
                                    Inventory.Weapon = weapon;
                                else if (pickedItem is Backpack backpack)
                                    Inventory.Backpack = backpack;
                                else
                                    Inventory.Backpack?.AddItem(pickedItem);
#if LOG
                                Logger.Log(LogEvent.Debug, "item picked", pickedItem);
#endif
                            }
                        }
                    }
                    else if (Inventory.Weapon.Update(this, controller.ShouldAct, totalGameTime))
                    {
                        var shootingDirection = controller.Target - animView.Position;
                        Shoot?.Invoke(this, new(shootingDirection, View.Position, Inventory.Weapon.Hit));
                    }
                    break;

                case CharacterView.State.Dead:
                    animView.SetFrame(totalGameTime);
                    break;

                default:
                    throw new NotSupportedException($"Unknown state: {charView.CurrentState}");
            }
        }

        public override void Draw(
#if DEBUG
            bool shouldShowCollisionBoxes,
#endif
            SpriteBatch spriteBatch,
            Camera camera
        )
        {
            base.Draw(
#if DEBUG
                shouldShowCollisionBoxes,
#endif
                spriteBatch,
                camera
            );
            Inventory.Weapon?.Draw(
#if DEBUG
                shouldShowCollisionBoxes,
#endif
                spriteBatch,
                camera
            );
        }

        private void OnItemDropped(object sender, CharactersInventory.DropItemEventArgs args)
        {
#if DEBUG
            if (sender != Inventory)
                throw new InvalidOperationException();
#endif
            ItemDropped?.Invoke(this, args);
        }

        public override void Dispose()
        {
            Shoot = null;
            ItemDropped = null;
            Inventory.ItemDropped -= OnItemDropped;
            Inventory.Dispose();
            base.Dispose();
        }

        public sealed class ShootingEventArgs
        {
            public Vector2 ShootingDirection { get; }
            public Vector2 StartPosition { get; }
            public int Hit { get; }

            public ShootingEventArgs(Vector2 shootingDirection, Vector2 startPosition, int hit)
            {
                ShootingDirection = shootingDirection;
                StartPosition = startPosition;
                Hit = hit;
            }
        }
    }
}
