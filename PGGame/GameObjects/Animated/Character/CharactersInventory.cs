﻿using System;
using System.Linq;

namespace PG
{
    public sealed class CharactersInventory : IDisposable
    {
        private Weapon weapon;
        private Backpack backpack;

        public bool IsArmed => weapon != null;

        public Backpack Backpack
        {
            get => backpack;
            set
            {
                backpack = value;
                if (backpack != null)
                    backpack.State = ItemView.State.Picked;
            }
        }

        public Weapon Weapon
        {
            get => weapon;
            set
            {
                weapon = value;
                if (weapon != null)
                    weapon.State = ItemView.State.Picked;
            }
        }

        public event EventHandler<DropItemEventArgs> ItemDropped;

        public void DropItem(long itemId)
        {
            var item = backpack?.Items.FirstOrDefault(item => item.Id == itemId);
            if (item != null)
            {
                DropItem(item);
                backpack.RemoveItem(item);
            }
            else if (Weapon?.Id == itemId)
            {
                DropItem(Weapon);
                Weapon = null;
            }
            else
                throw new ArgumentException($"No items found with id {itemId}");
        }

        private void DropItem(Item item)
        {
            item.State = ItemView.State.Idle;
            ItemDropped?.Invoke(this, new(item));
        }

        public bool Arm()
        {
            if (Weapon == null)
            {
                var weapon = backpack?.GetFirstItemByType<Weapon>();
                if (weapon != null)
                {
                    backpack.RemoveItem(weapon);
                    Weapon = weapon;
                    return true;
                }
            }
            return false;
        }

        public bool Disarm()
        {
            if (Weapon != null && Backpack != null)
            {
                backpack.AddItem(Weapon);
                Weapon = null;
                return true;
            }
            return false;
        }

        public void Dispose()
        {
            Backpack?.Dispose();
            Weapon?.Dispose();
        }

        public sealed class DropItemEventArgs
        {
            public Item Item { get; }

            public DropItemEventArgs(Item item)
            {
                Item = item;
                Item.State = ItemView.State.Idle;
            }
        }
    }
}
