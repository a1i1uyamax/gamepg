﻿using System;
using Microsoft.Xna.Framework;

namespace PG
{
    public sealed class Weapon : Item
    {
        private TimeSpan previousGameTime;
        private TimeSpan rateOfFire;

        public int Hit { get; set; }

        public double RateOfFire
        {
            get => rateOfFire.TotalSeconds;
            set => rateOfFire = TimeSpan.FromMilliseconds(value * 1E+3);
        }

        public Weapon(ItemView animView, string name) : base(animView, name)
        {
        }

        public bool Update(Character owner,
                           bool shooting,
                           TimeSpan totalGameTime)
        {
            animView.Origin = owner.View.Origin;
            animView.Rotation = owner.View.Rotation + MathHelper.Pi;
            animView.Position = owner.View.Position;

            if (shooting && totalGameTime - previousGameTime > rateOfFire)
            {
                animView.SetFrame();
                previousGameTime = totalGameTime;
                return true;
            }

            animView.ResetTextureRect();
            return false;
        }
    }
}
