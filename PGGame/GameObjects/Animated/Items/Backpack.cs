﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PG
{
    public sealed class Backpack : Item
    {
        private readonly List<Item> items = new();

        public bool ContainsWeapon => items.Any(item => item is Weapon);
        public IEnumerable<Item> Items => items;

        public Backpack(ItemView view, string name) : base(view, name)
        {
        }

        public TItem GetFirstItemByType<TItem>() where TItem : Item
            => items.FirstOrDefault(item => item is TItem) as TItem;

        public void RemoveItem(Item item)
        {
            item.State = ItemView.State.Idle;
            items.Remove(item);
        }

        public void AddItem(Item item)
        {
            item.State = ItemView.State.Picked;
            items.Add(item);
        }

        public override void Update(TimeSpan totalGameTime, Character owner = null)
        {
            base.Update(totalGameTime, owner);
            items.ForEach(item => item.Update(totalGameTime, owner));
        }

        public override void Dispose()
        {
            items.Clear();
            base.Dispose();
        }
    }
}
