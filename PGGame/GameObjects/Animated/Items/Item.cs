﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public class Item : AnimatedGameObject
    {
        private readonly ItemView itemView;

        public ItemView.State State
        {
            get => itemView.CurrentState;
            set => itemView.CurrentState = value;
        }
        public Texture2D IconTexture => itemView.IconTexture;

        public Item(ItemView itemView, string name) : base(itemView, name, IdManager.GenerateId())
            => this.itemView = itemView;

        public virtual void Update(TimeSpan totalGameTime, Character owner = null)
        {
            if (owner == null)
            {
                animView.SetFrame(totalGameTime);
            }
            else
                animView.Position = owner.View.Position;
        }
    }
}
