﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public class GameObject : IDisposable
    {
        public long Id { get; }

        public View View { get; }

        public string Name { get; }

        public GameObject(View view, string name, long id)
            => (View, Name, Id) = (view, name ?? GameObjectsConst.DefaultName, id);

        public virtual void Draw(
#if DEBUG
            bool shouldShowCollisionBoxes,
#endif
            SpriteBatch spriteBatch,
            Camera camera
        )
        {
            View.DrawIfVisible(
#if DEBUG
                shouldShowCollisionBoxes,
#endif
                spriteBatch,
                camera
            );
        }

        public virtual void Dispose()
        {
#if LOG
            Logger.Log(LogEvent.Debug, "disposed", this);
#endif
        }
    }
}
