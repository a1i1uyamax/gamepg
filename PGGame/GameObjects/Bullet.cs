﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace PG
{
    public sealed class Bullet : GameObject
    {
        private readonly Rectangle mapRect;
        private readonly int hit;
        private Vector2 step;

        public bool IsActive { get; private set; }
        public static BulletsFactory Factory { get; private set; }

        public Bullet(View view, Rectangle mapRect) : base(view, string.Empty, IdManager.GenerateId())
            => this.mapRect = mapRect;

        public Bullet(View view, Rectangle mapRect, int hit, Vector2 step) : this(view, mapRect)
        {
            this.hit = hit;
            this.step = step;
            IsActive = true;
        }

        public void Update(IEnumerable<GameObject> staticObjects, IEnumerable<Character> characters)
        {
            var newPosition = View.Position + step;

            foreach (var character in characters.Where(ch => ch.IsAlive))
            {
                if (View.GetRectAt(newPosition).Intersects(character.View.GetTextureRectWithPaddings()))
                {
                    character.Health -= hit;
                    Dispose();
                    return;
                }
            }
            foreach (var staticObject in staticObjects)
            {
                if (View.GetRectAt(newPosition).Intersects(staticObject.View.GetTextureRect()))
                {
                    Dispose();
                    return;
                }
            }
            if (!mapRect.Intersects(View.GetRectAt(newPosition)))
            {
                Dispose();
                return;
            }

            View.Position = newPosition;
        }

        public override void Dispose()
        {
            base.Dispose();
            IsActive = false;
        }

        public static void InitializeFactory(Bullet bulletInstance)
            => Factory = new(bulletInstance);

        public sealed class BulletsFactory
        {
            private readonly Bullet bulletInstance;

            public BulletsFactory(Bullet bulletInstance)
                => this.bulletInstance = bulletInstance;

            public Bullet New(Vector2 direction,
                              Vector2 position,
                              int hit,
                              float speed = GameObjectsConst.BulletDefaultSpeed)
            {
                var step = Vector2.Normalize(direction) * speed;
                var result = new Bullet(new(bulletInstance.View), bulletInstance.mapRect, hit, step);
                result.View.Position = position;
                return result;
            }
        }
    }
}
