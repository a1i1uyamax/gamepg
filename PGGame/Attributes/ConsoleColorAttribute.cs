﻿using System;

namespace PG
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public sealed class ConsoleColorAttribute : Attribute, IValueAttribute<ConsoleColor>
    {
        public ConsoleColor Value { get; }

        public ConsoleColorAttribute(ConsoleColor value)
            => Value = value;
    }
}
