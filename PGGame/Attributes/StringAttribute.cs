﻿using System;

namespace PG
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public sealed class StringAttribute : Attribute, IValueAttribute<string>
    {
        public string Value { get; }

        public StringAttribute(string value)
            => Value = value;
    }
}
