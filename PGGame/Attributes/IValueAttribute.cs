﻿namespace PG
{
    public interface IValueAttribute<TValue>
    {
        TValue Value { get; }
    }
}
