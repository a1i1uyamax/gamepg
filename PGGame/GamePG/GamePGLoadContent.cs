﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed partial class GamePG : Game
    {
        protected override void LoadContent()
        {
            spriteBatch = new(GraphicsDevice);
            kbManager = InputManager.LoadFromConfig(Content, "input");
            pauseTextFont = Content.Load<SpriteFont>("font/pause");
            inventoryMenu = InventoryMenu.Load(Content);

            // --------------------------------------------------------------------------------------

            groundView = ViewLoader.LoadView<GroundView>(Content, "ground");
            groundView.VerticalCount = groundView.HorizontalCount = 10;
            groundView.Position = new Vector2(groundView.Texture.Width, groundView.Texture.Height) * -5;

            // --------------------------------------------------------------------------------------

            var flower = new Item(ViewLoader.LoadItemView(Content, "flower/flower", "flower/flower", "flower/flower"), null);
            flower.View.Position = new(-50);
            idleItems.Add(flower);

            // --------------------------------------------------------------------------------------

            var bulletInstance = new Bullet(ViewLoader.LoadView(Content, "bullet/bullet"), groundView.GetMapRect());
            Bullet.InitializeFactory(bulletInstance);

            // --------------------------------------------------------------------------------------

            var wall = new GameObject(ViewLoader.LoadView(Content, "wall"), string.Empty, IdManager.GenerateId());
            wall.View.Position = new(800);
            staticObjects.Add(wall);

            // --------------------------------------------------------------------------------------

            var weapon0 = new Weapon(ViewLoader.LoadItemView(Content, "weapon0/weapon0_idle", "weapon0/weapon0", "weapon0/weapon0_icon"), "Pistol")
            {
                Hit = 50,
                RateOfFire = 1.0
            };
            weapon0.View.Position = new(700);
            idleItems.Add(weapon0);

            // --------------------------------------------------------------------------------------

            mainCharacter = new(
                ViewLoader.LoadCharacterView(
                    Content,
                    "main_character_0/main_character_0",
                    "main_character_0/main_character_0_corpse"
                ),
                null,
                IdManager.GenerateId()
            )
            {
                Speed = 5f,
                Health = 100,
                PickUpDistance = 50
            };
            mainCharacter.View.Position = new Vector2(ScreenWidth, ScreenHeight) / 2f - mainCharacter.View.Origin;
            mainCharacter.Shoot += OnCharacterShot;
            mainCharacter.ItemDropped += OnCharacterDroppedItem;

            // --------------------------------------------------------------------------------------

            var backpack = new Backpack(ViewLoader.LoadItemView(Content, "backpack/backpack", "backpack/backpack", "backpack/backpack"), "Backpack");
            backpack.View.Position = new(mainCharacter.View.Position.X + 100, mainCharacter.View.Position.Y);
            idleItems.Add(backpack);

            // --------------------------------------------------------------------------------------

            var character0 = new Character(
                ViewLoader.LoadCharacterView(
                    Content,
                    "character_0/character_0",
                    "character_0/character_0_corpse"
                ),
                "Pisikak",
                IdManager.GenerateId()
            )
            {
                Speed = 5f,
                Health = 100
            };
            character0.View.Position = new(-150);
            characters.Add(character0);
            controllers.Add(character0.Id, defaultController);

            // --------------------------------------------------------------------------------------

            var character1 = new Character(
                ViewLoader.LoadCharacterView(
                    Content,
                    "character_0/character_0",
                    "character_0/character_0_corpse"
                ),
                "Fucked up",
                IdManager.GenerateId()
            )
            {
                Speed = 5f,
                Health = 100
            };
            character1.View.Position = new(-200);
            characters.Add(character1);
            controllers.Add(character1.Id, new GoToTargetController(new(100, 100), new(-500, 100), new(-500, -500), new(100, -500)));

            // --------------------------------------------------------------------------------------

            base.LoadContent();
        }
    }
}
