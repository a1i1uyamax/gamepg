﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed partial class GamePG : Game
    {
        private enum GameState
        {
            Running,
            Pause,
            CharacterMenu,
        }

        private readonly Camera camera;
        private readonly GraphicsDeviceManager gdManager;
        private readonly List<GameObject> staticObjects = new();
        private readonly List<Item> idleItems = new();
        private readonly List<Character> characters = new();
        private readonly List<Bullet> bullets = new();
        private readonly DefaultController defaultController;
        private readonly MainCharacterController mainCharacterController;
        private readonly Dictionary<long, IController> controllers = new();
        private readonly IntentHandler pauseHandler = new(InputIntent.Pause);
        private readonly IntentHandler inventoryMenuHandler = new(InputIntent.CharacterMenu);
#if DEBUG
        private readonly IntentHandler shouldShowCollisionBoxesHandler = new(InputIntent.ShowCollisionBoxes);
        private bool collisionBoxesShowing = false;
#endif

        private InventoryMenu inventoryMenu;
        private SpriteFont pauseTextFont;
        private GroundView groundView;
        private Character mainCharacter;
        private InputManager kbManager;
        private SpriteBatch spriteBatch;
        private GameState state;

        private int ScreenWidth => gdManager.PreferredBackBufferWidth;
        private int ScreenHeight => gdManager.PreferredBackBufferHeight;

        public GamePG()
        {
            Content.RootDirectory = "Content";

            gdManager = new GraphicsDeviceManager(this)
            {
#if DEBUG
                IsFullScreen = false,
                PreferredBackBufferWidth = 1280,
                PreferredBackBufferHeight = 720
#else
                IsFullScreen = true,
                PreferredBackBufferWidth = 1920,
                PreferredBackBufferHeight = 1080
#endif
            };
            camera = new(gdManager);
            defaultController = new(Vector2.Zero);
            mainCharacterController = new();
            state = GameState.Running;

            IsMouseVisible = true;
            TargetElapsedTime = TimeSpan.FromSeconds(1.0 / 60.0);
        }

#if DEBUG
        protected override void Initialize()
        {
            CollisionBox.Initialize(GraphicsDevice);
            base.Initialize();
        }

        protected override void Dispose(bool disposing)
        {
            CollisionBox.Deinitialize();
            base.Dispose(disposing);
        }
#endif

        protected override void OnExiting(object sender, EventArgs args)
        {
            inventoryMenu.Dispose();
            staticObjects.ForEach(staticObject => staticObject.Dispose());
            staticObjects.Clear();
            idleItems.ForEach(item => item.Dispose());
            idleItems.Clear();
            characters.ForEach(character => character.Dispose());
            characters.Clear();
            bullets.ForEach(bullet => bullet.Dispose());
            bullets.Clear();
            mainCharacter.Dispose();
            base.OnExiting(sender, args);
        }

        private void OnCharacterShot(object sender, Character.ShootingEventArgs args)
        {
#if DEBUG
            if (sender is not Character)
                throw new InvalidOperationException();
#endif
            var bullet = Bullet.Factory.New(args.ShootingDirection, args.StartPosition, args.Hit);
            bullets.Add(bullet);
        }

        private void OnCharacterDroppedItem(object sender, CharactersInventory.DropItemEventArgs args)
        {
#if DEBUG
            if (sender is not Character)
                throw new InvalidOperationException();
#endif
            idleItems.Add(args.Item);
        }
    }
}
