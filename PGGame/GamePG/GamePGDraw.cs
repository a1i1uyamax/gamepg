﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed partial class GamePG : Game
    {
        protected override void Draw(GameTime gameTime)
        {
            void DrawWorld()
            {
                groundView.Draw(
#if DEBUG
                    collisionBoxesShowing,
#endif
                    spriteBatch
                );
                staticObjects.ForEach(staticObject =>
                    staticObject.Draw(
#if DEBUG
                        collisionBoxesShowing,
#endif
                        spriteBatch,
                        camera
                    )
                );
                idleItems.ForEach(item =>
                    item.Draw(
#if DEBUG
                        collisionBoxesShowing,
#endif
                        spriteBatch,
                        camera
                    )
                );
                characters.ForEach(character =>
                    character.Draw(
#if DEBUG
                        collisionBoxesShowing,
#endif
                        spriteBatch,
                        camera
                    )
                );
                mainCharacter.Draw(
#if DEBUG
                    collisionBoxesShowing,
#endif
                    spriteBatch,
                    camera
                );
                bullets.ForEach(bullet =>
                    bullet.Draw(
#if DEBUG
                        collisionBoxesShowing,
#endif
                        spriteBatch,
                        camera
                    )
                );
            }

            if (IsActive)
            {
                GraphicsDevice.Clear(Color.White);

                spriteBatch.Begin(
                    SpriteSortMode.Deferred,
                    BlendState.AlphaBlend,
                    SamplerState.LinearWrap,
                    DepthStencilState.None,
                    RasterizerState.CullNone,
                    null,
                    camera.Transform
                );

                switch (state)
                {
                    case GameState.Running:
                        DrawWorld();
                        break;

                    case GameState.CharacterMenu:
                        inventoryMenu.Draw(
#if DEBUG
                            collisionBoxesShowing,
# endif
                            spriteBatch
                        );
                        break;

                    case GameState.Pause:
                        DrawWorld();
                        spriteBatch.DrawString(pauseTextFont, "PAUSE", new(ScreenWidth / 2, ScreenHeight / 2), Color.Red);
                        break;

                    default:
                        throw new NotSupportedException($"Unknown state: {state}");
                }

                spriteBatch.End();

                base.Draw(gameTime);
            }
        }
    }
}
