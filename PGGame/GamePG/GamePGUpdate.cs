﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PG
{
    public sealed partial class GamePG : Game
    {
        protected override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                var mouseState = Mouse.GetState();
                var keyboardState = Keyboard.GetState();
                var intents = kbManager.GetIntents(keyboardState, mouseState);
                pauseHandler.Update(gameTime.TotalGameTime, intents);
                inventoryMenuHandler.Update(gameTime.TotalGameTime, intents);
#if DEBUG
                shouldShowCollisionBoxesHandler.Update(gameTime.TotalGameTime, intents);
                collisionBoxesShowing ^= shouldShowCollisionBoxesHandler.AllowIntent;
#endif

                switch (state)
                {
                    case GameState.Running:
                        var relativeMousePosition = camera.GetRelativeMousePosition(mouseState);
                        var mainCharacterPreviousPosition = mainCharacter.View.Position;
                        mainCharacterController.Update(intents, relativeMousePosition, gameTime.TotalGameTime);

                        characters.ForEach(character => character.Update(gameTime.TotalGameTime, staticObjects, idleItems, controllers[character.Id]));
                        mainCharacter.Update(gameTime.TotalGameTime, staticObjects, idleItems, mainCharacterController);

                        bullets.RemoveAll(bullet => !bullet.IsActive);
                        bullets.ForEach(bullet => bullet.Update(staticObjects, characters));

                        idleItems.RemoveAll(item => item.State == ItemView.State.Picked);
                        idleItems.ForEach(item => item.Update(gameTime.TotalGameTime));

                        var cameraDisplacement = (relativeMousePosition - mainCharacter.View.Position) / 2f;
                        var cameraShift = mainCharacter.View.Position - mainCharacterPreviousPosition;
                        camera.Update(GraphicsDevice.Viewport, mouseState, cameraDisplacement, cameraShift);
                        break;

                    case GameState.CharacterMenu:
                        inventoryMenu.Update(mouseState, camera);
                        break;

                    case GameState.Pause:
                        break;

                    default:
                        throw new NotSupportedException($"Unknown state: {state}");
                }

                if (pauseHandler.AllowIntent)
                {
                    state = state switch
                    {
                        GameState.Pause => GameState.Running,
                        GameState.Running => GameState.Pause,
                        _ => state
                    };
                }
                if (inventoryMenuHandler.AllowIntent)
                {
                    switch (state)
                    {
                        case GameState.Running:
                            state = GameState.CharacterMenu;
                            inventoryMenu.Start(camera, mainCharacter);
                            break;
                        case GameState.CharacterMenu:
                            state = GameState.Running;
                            inventoryMenu.End();
                            break;
                        default:
                            break;
                    }
                }
#if !DEBUG
                if (intents.CheckInputIntent(InputIntent.Escape))
                    Exit();
#endif
                base.Update(gameTime);
            }
        }
    }
}
