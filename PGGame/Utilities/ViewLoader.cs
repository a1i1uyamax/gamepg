﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public static class ViewLoader
    {
        private static readonly string AssetNameFormat = "textures/{0}";

        public static View LoadView(
            ContentManager content,
            string assetName,
            Vector2? origin = null,
            float rotation = 0f
        )
        {
            var texture = LoadTexture(content, assetName);
            return new(texture, origin, rotation);
        }

        public static TView LoadView<TView>(
            ContentManager content,
            string assetName,
            Vector2? origin = null,
            float rotation = 0f
        ) where TView : View
        {
            var texture = LoadTexture(content, assetName);
            return Activator.CreateInstance(typeof(TView), texture, origin, rotation) as TView;
        }

        public static AnimatedView LoadAnimatedView(
            ContentManager content,
            string assetName
        )
        {
            var animViewTexture = LoadAnimViewTexture(content, assetName);
            return new(animViewTexture);
        }

        public static CharacterView LoadCharacterView(
            ContentManager content,
            string mainAssetName,
            string deadAssetName
        )
        {
            var mainAnimViewTexture = LoadAnimViewTexture(content, mainAssetName);
            var deadAnimViewTexture = LoadAnimViewTexture(content, deadAssetName);
            return new(mainAnimViewTexture, deadAnimViewTexture);
        }

        public static ItemView LoadItemView(
            ContentManager content,
            string idleAssetName,
            string pickedAssetName,
            string iconAssetName
        )
        {
            var idleAnimViewTexture = LoadAnimViewTexture(content, idleAssetName);
            var pickedAnimViewTexture = LoadAnimViewTexture(content, pickedAssetName);
            var iconTexture = LoadTexture(content, iconAssetName);
            return new(idleAnimViewTexture, pickedAnimViewTexture, iconTexture);
        }

        private static AnimatedViewTexture LoadAnimViewTexture(
            ContentManager content,
            string assetName
        )
        {
            var texture = LoadTexture(content, assetName);
            var textureInfo = JsonParser.ParseFromContentFile<AnimatedViewInfo>(content, GetNameFormatted(assetName));
            return new(texture, textureInfo);
        }

        private static Texture2D LoadTexture(ContentManager content, string assetName)
            => content.Load<Texture2D>(GetNameFormatted(assetName));

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetNameFormatted(string assetName)
            => string.Format(AssetNameFormat, assetName);
    }
}
