﻿#if DEBUG
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PG
{
    public sealed class CollisionBox : IDisposable
    {
        private const int TextureSize = 1;
        private static readonly object locker = new();

        private static CollisionBox instance;

        public static CollisionBox Instance
        {
            get
            {
                lock (locker)
                {
                    return instance ?? throw new NullReferenceException("Instance is not initialized yet");
                }
            }
        }

        public static void Initialize(GraphicsDevice graphicsDevice)
        {
            lock (locker)
            {
                instance = new(graphicsDevice);
            }
        }

        public static void Deinitialize()
        {
            lock (locker)
            {
                instance?.Dispose();
                instance = null;
            }
        }

        public Texture2D Texture { get; private set; }

        private CollisionBox(GraphicsDevice graphicsDevice)
        {
            Texture = new(graphicsDevice, TextureSize, TextureSize);
            Texture.SetData(new Color[] { Color.Yellow });
        }

        public void Dispose()
        {
            Texture?.Dispose();
            Texture = null;
        }
    }
}
#endif
