﻿using System;
using System.Linq;

namespace PG
{
    public static class EnumExtensions
    {
        public static bool CheckInputIntent(this InputIntent intents, InputIntent neededIntent)
            => (intents & neededIntent) != 0x0;

        public static TEnum Parse<TEnum>(string value) where TEnum : Enum
            => (TEnum)Enum.Parse(typeof(TEnum), value);

        public static string GetStringValue(this Enum value)
            => GetAttributeRefValue<string, StringAttribute>(value)
                ?? throw new NullReferenceException("No StringAttribute value found");

        public static ConsoleColor GetConsoleColorValue(this Enum value)
            => GetAttributeValue<ConsoleColor, ConsoleColorAttribute>(value)
                ?? throw new NullReferenceException("No ConsoleColorAttribute value found");

        private static TRefValue GetAttributeRefValue<TRefValue, TAttribute>(this Enum value)
            where TRefValue : class
            where TAttribute : IValueAttribute<TRefValue>
        {
            return value.GetType()
                        .GetField(value.ToString())
                        ?.GetCustomAttributes(typeof(TAttribute), false)
                        ?.FirstOrDefault() is TAttribute attr
                            ? attr.Value
                            : null;
        }

        private static TValue? GetAttributeValue<TValue, TAttribute>(this Enum value)
           where TValue : struct
           where TAttribute : IValueAttribute<TValue>
        {
            return value.GetType()
                        .GetField(value.ToString())
                        ?.GetCustomAttributes(typeof(TAttribute), false)
                        ?.FirstOrDefault() is TAttribute attr
                            ? attr.Value
                            : null;
        }
    }
}
