﻿using System;

namespace PG
{
    public sealed class IntentHandler
    {
        private static readonly TimeSpan Delay = TimeSpan.FromMilliseconds(250.0);

        private readonly InputIntent inputIntent;

        private InputIntent inputIntents;
        private TimeSpan lastIntentTime;
        private TimeSpan totalGameTime;

        public bool AllowIntent
        {
            get
            {
                if (totalGameTime > lastIntentTime + Delay
                    && inputIntents.CheckInputIntent(inputIntent))
                {
                    lastIntentTime = totalGameTime;
                    return true;
                }
                return false;
            }
        }

        public IntentHandler(InputIntent inputIntent)
            => this.inputIntent = inputIntent;

        public void Update(TimeSpan totalGameTime, InputIntent inputIntents)
            => (this.totalGameTime, this.inputIntents) = (totalGameTime, inputIntents);
    }
}
