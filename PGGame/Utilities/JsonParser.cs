﻿using System.IO;
using Microsoft.Xna.Framework.Content;
using Newtonsoft.Json;

namespace PG
{
    public static class JsonParser
    {
        public static T ParseFromContentFile<T>(ContentManager content, string fileName)
        {
            using var reader = new StreamReader($"{content.RootDirectory}/{fileName}.json");
            var json = reader.ReadToEnd();
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
