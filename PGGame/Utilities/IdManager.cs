﻿namespace PG
{
    public static class IdManager
    {
        public const long InvalidId = -1L;

        private static long lastId = 0L;

        public static long GenerateId() => ++lastId;
    }
}
