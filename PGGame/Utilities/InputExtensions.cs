﻿using System;
using Microsoft.Xna.Framework.Input;

namespace PG
{
    public static class InputExtensions
    {
        public static bool IsKeyDown(this MouseState mouseState, MouseKeys key) => key switch
        {
            MouseKeys.Left => mouseState.LeftButton == ButtonState.Pressed,
            MouseKeys.Right => mouseState.RightButton == ButtonState.Pressed,
            MouseKeys.Middle => mouseState.MiddleButton == ButtonState.Pressed,
            MouseKeys.XButton1 => mouseState.XButton1 == ButtonState.Pressed,
            MouseKeys.XButton2 => mouseState.XButton2 == ButtonState.Pressed,
            _ => throw new ArgumentException($"Unknown mouse key: {key}")
        };
    }
}
