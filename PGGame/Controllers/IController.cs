﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace PG
{
    public interface IController
    {
        Vector2 Target { get; }
        bool ShouldAct { get; }
        bool ShouldChangeMode { get; }
        Vector2 GetDirectionVector(Character character);
        Vector2 GetNewPosition(Character character, IEnumerable<GameObject> staticObjects);
    }
}
