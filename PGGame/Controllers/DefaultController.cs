﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace PG
{
    public sealed class DefaultController : IController
    {
        public Vector2 Target { get; }

        public bool ShouldAct => false;

        public bool ShouldChangeMode => false;

        public DefaultController(Vector2 direction)
            => Target = direction;

        public Vector2 GetDirectionVector(Character _)
            => Vector2.Zero;

        public Vector2 GetNewPosition(Character character, IEnumerable<GameObject> _)
            => character.View.Position;
    }
}
