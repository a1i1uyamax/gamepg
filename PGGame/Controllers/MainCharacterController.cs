﻿using System;
using Microsoft.Xna.Framework;

namespace PG
{
    public sealed class MainCharacterController : AbstractCharacterController
    {
        private readonly IntentHandler changeModeHandler;

        private Vector2 relativeMousePosition;
        private InputIntent inputIntents;

        public override Vector2 Target => relativeMousePosition;

        public override bool ShouldAct => inputIntents.CheckInputIntent(InputIntent.Act);

        public override bool ShouldChangeMode => changeModeHandler.AllowIntent;

        public MainCharacterController()
            => changeModeHandler = new(InputIntent.ChangeMode);

        public override Vector2 GetDirectionVector(Character character)
        {
            var forwardVector = inputIntents.CheckInputIntent(InputIntent.MoveForward)
               ? relativeMousePosition - character.View.Position
               : Vector2.Zero;
            var backVector = inputIntents.CheckInputIntent(InputIntent.MoveBack)
                ? character.View.Position - relativeMousePosition
                : Vector2.Zero;
            var rightVector = inputIntents.CheckInputIntent(InputIntent.MoveRight)
                ? Vector2.Transform(relativeMousePosition - character.View.Position, MathConst.RightTurnQuaternion)
                : Vector2.Zero;
            var leftVector = inputIntents.CheckInputIntent(InputIntent.MoveLeft)
                ? Vector2.Transform(relativeMousePosition - character.View.Position, MathConst.LeftTurnQuaternion)
                : Vector2.Zero;
            return forwardVector + backVector + rightVector + leftVector;
        }

        public void Update(InputIntent inputIntents, Vector2 relativeMousePosition, TimeSpan totalGameTime)
        {
            this.inputIntents = inputIntents;
            this.relativeMousePosition = relativeMousePosition;
            changeModeHandler.Update(totalGameTime, inputIntents);
        }
    }
}
