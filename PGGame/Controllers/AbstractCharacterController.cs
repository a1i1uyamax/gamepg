﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace PG
{
    public abstract class AbstractCharacterController : IController
    {
        public abstract Vector2 Target { get; }

        public abstract Vector2 GetDirectionVector(Character character);

        public abstract bool ShouldAct { get; }

        public virtual bool ShouldChangeMode => false;

        public Vector2 GetNewPosition(Character character, IEnumerable<GameObject> staticObjects)
        {
            var newPosition = character.View.Position;
            var directionVector = GetDirectionVector(character);
            if (directionVector != Vector2.Zero)
            {
                var normDirectionVector = Vector2.Normalize(directionVector);
                var step = normDirectionVector * character.Speed;
                var rect = character.View.GetRectWithPaddingAt(character.View.Position + step);

                var intersectedObject = staticObjects.FirstOrDefault(obj => rect.Intersects(obj.View.GetTextureRect()));
                if (intersectedObject != null)
                {
                    var correctedByYStep = new Vector2(step.X, 0f);
                    rect = character.View.GetRectWithPaddingAt(character.View.Position + correctedByYStep);
                    if (rect.Intersects(intersectedObject.View.GetTextureRect()))
                    {
                        var correctedByXStep = new Vector2(0f, step.Y);
                        rect = character.View.GetRectWithPaddingAt(character.View.Position + correctedByXStep);
                        step = rect.Intersects(intersectedObject.View.GetTextureRect())
                            ? -normDirectionVector : correctedByXStep;
                    }
                    else
                        step = correctedByYStep;
                }
                Update(character, step);
                return character.View.Position + step;
            }
            return newPosition;
        }

        protected virtual void Update(Character character, Vector2 step)
        {
            // Nothing to do by default
        }
    }
}
