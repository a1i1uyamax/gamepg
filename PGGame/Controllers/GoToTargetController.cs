﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace PG
{
    public sealed class GoToTargetController : AbstractCharacterController
    {
        private const int TargetRectSize = 50;

        private readonly List<Vector2> targets = new();

        private int currentTargetIndex = 0;
        private Rectangle targetRect;

        public override Vector2 Target => targets[currentTargetIndex];

        public override bool ShouldAct => false;

        public GoToTargetController(params Vector2[] targets)
        {
            this.targets.AddRange(targets);
            targetRect = GetTargetRect(targets[currentTargetIndex]);
        }

        public override Vector2 GetDirectionVector(Character character)
            => Target - character.View.Position;

        protected override void Update(Character character, Vector2 step)
        {
            if (targetRect.Intersects(character.View.GetRectAt(character.View.Position + step)))
            {
                var newIndex = currentTargetIndex + 1;
                currentTargetIndex = newIndex < targets.Count ? newIndex : 0;
                targetRect = GetTargetRect(targets[currentTargetIndex]);
            }
        }

        private static Rectangle GetTargetRect(Vector2 target)
            => new((int)target.X - TargetRectSize / 2,
                   (int)target.Y - TargetRectSize / 2,
                   TargetRectSize,
                   TargetRectSize);
    }
}
